package chat.server;

import chat.client.ClientContext;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class Server {

     private static final int CONNECTIONS_LIMIT = 255;
     private final ServerSocket serverSocket;
     private List<ClientContext> connectedClients = new LinkedList<>();

     public void listen() {
          while (true) {
               acceptConnection();
          }
     }

     private void acceptConnection() {
          log.info("Listening on port " + serverSocket.getLocalPort());
          final Socket clientSocket = Try.of(serverSocket::accept).getOrNull();
          if (connectedClients.size() < CONNECTIONS_LIMIT) {
               connect(clientSocket);
          } else {
               log.warn("Reached maximum number of connected clients!");
               Try.run(clientSocket::close);
          }
     }

     private void connect(Socket clientSocket) {
          log.info("Connecting new client");
          final ClientContext client = new ClientContext(clientSocket, connectedClients);
          connectedClients.add(client);
          Thread thread = new Thread(client);
          thread.setDaemon(true);
          thread.start();
     }
}
