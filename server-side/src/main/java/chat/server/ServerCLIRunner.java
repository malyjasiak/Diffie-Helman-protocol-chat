package chat.server;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.function.Predicate;

@Slf4j
public class ServerCLIRunner {

     private static final String PORT_PROMPT_MESSAGE = "Port number to listen at: ";

     private final static Predicate<Integer> isValidPort = port -> port >= 1024 && port <= 65535;

     public static void run() {
          ServerSocket socket = null;
          while (socket == null) {
               socket = Try.of(ServerCLIRunner::initializeServerSocket).getOrNull();
          }
          log.info("Opened server socket");
          new Server(socket).listen();
     }

     private static ServerSocket initializeServerSocket() throws Exception {
          BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
          final Integer port = readValidPort(br);
          return new ServerSocket(port);
     }

     private static Integer readValidPort(BufferedReader br) throws Exception {
          System.out.print(PORT_PROMPT_MESSAGE);
          return Try.of(br::readLine)
                    .map(Integer::parseInt)
                    .filter(isValidPort)
                    .getOrElseThrow(throwable -> new IllegalArgumentException(throwable));
     }
}
