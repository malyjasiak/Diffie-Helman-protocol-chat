package chat.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;

public class JsonMapper {

     private static JsonMapper instance = new JsonMapper();
     @Getter
     private ObjectMapper objectMapper;

     private JsonMapper() {
          this.objectMapper = new ObjectMapper();
     }

     public static JsonMapper getInstance() {
          return instance;
     }
}
