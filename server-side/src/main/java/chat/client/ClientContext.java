package chat.client;

import protocol.model.ProtocolParams;
import protocol.ProtocolUtil;
import protocol.util.Common;
import chat.util.JsonMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import encryption.EncryptionWorker;
import encryption.common.RandomGenerator;
import encryption.model.Encryption;
import encryption.model.EncryptionDTO;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import protocol.model.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.List;
import java.util.Optional;

import static protocol.util.Common.KEYS_REQUEST_VALUE;

@Slf4j
public class ClientContext implements Runnable {

     private final Socket socket;
     private final List<ClientContext> connectedClients;
     private Encryption encryption = Encryption.None;
     private ProtocolParams protocolParameters;
     private BufferedReader bufferedReader;
     private PrintStream printStream;

     private ObjectMapper objectMapper = JsonMapper.getInstance().getObjectMapper();

     public ClientContext(Socket socket, List<ClientContext> connectedClients) {
          this.socket = socket;
          this.connectedClients = connectedClients;
          protocolParameters = new ProtocolParams();
     }

     @Override
     public void run() {
          Try.run(this::startChat)
             .onFailure(throwable -> log.error("Error handling client connection", throwable))
             .andThenTry(this::closeConnection);
     }

     private void startChat() throws Exception {
          InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
          bufferedReader = new BufferedReader(inputStreamReader);
          printStream = new PrintStream(socket.getOutputStream());

          final String clientRequest = bufferedReader.readLine();
          ClientRequestDTO clientRequestDTO = objectMapper.readValue(clientRequest, ClientRequestDTO.class);
          if (!clientRequestDTO.getRequest().equals(KEYS_REQUEST_VALUE)) {
               log.warn("Couldn't handle connection attempt @ stage 1");
               throw new IOException();
          }
          sendServerProtocolParameters();
          listenForClientProtocolParams();
          computeSParameter();

          listen();
          closeConnection();
     }

     private void computeSParameter() {
          long commonEncKey = ProtocolUtil.computeCommonEncKey(protocolParameters);
          protocolParameters.setCommonEncKey(commonEncKey);
     }

     private void sendServerProtocolParameters() throws JsonProcessingException {
          Thread thread = new Thread(() -> Try.run(this::provideServerParams)
                                              .onFailure(throwable -> log.error(throwable.toString())));
          thread.setDaemon(true);
          thread.start();
     }

     private void provideServerParams() throws Exception{
          providePGParams();
          provideBParam();
     }

     private void providePGParams() throws JsonProcessingException {
          long p = RandomGenerator.generatePrimeLong();
          long g = RandomGenerator.generatePrimitiveRootModulo(p);

          send(PGParametersDTO.builder()
                              .p(p)
                              .g(g)
                              .build());

          protocolParameters.setP(p);
          protocolParameters.setG(g);
     }

     private void provideBParam() throws JsonProcessingException {
          long privateB = RandomGenerator.generatePositiveLong();
          protocolParameters.setPrivateKey(privateB);

          long publicKey = ProtocolUtil.computePublicKey(protocolParameters);

          send(BParameterDTO.builder()
                            .b(publicKey)
                            .build());

          protocolParameters.setPublicKey(publicKey);
     }

     private void listenForClientProtocolParams() throws IOException {
          readAParam();
     }

     private void readAParam() throws IOException {
          final String clientAParam = bufferedReader.readLine();
          AParameterDTO aParameterDTO = objectMapper.readValue(clientAParam, AParameterDTO.class);
          long receivedKey = aParameterDTO.getA();

          protocolParameters.setReceivedKey(receivedKey);
     }

     private void listen() throws IOException {
          String line;
          while ((line = Try.of(bufferedReader::readLine).getOrNull()) != null) {
               JSONObject jsonObject = new JSONObject(line);
               boolean isEncryptionRequest = jsonObject.keySet().contains(Common.ENCRYPTION_KEY);
               if(isEncryptionRequest) {
                    setUpEncryption(line);
                    continue;
               }
               MessageDTO messageDTO = objectMapper.readValue(line, MessageDTO.class);
               log.info("Received message");
               MessageDTO decryptedMessage = decryptMessage(messageDTO);
               synchronized (this) {
                    for (ClientContext connectedClient : connectedClients) {
                         connectedClient.encryptAndSendMessage(decryptedMessage);
                    }
               }
          }
     }

     private void closeConnection() {
          Try.run(() -> {
               log.info("Closing connection");
               Optional.ofNullable(printStream).ifPresent(PrintStream::close);
               Optional.ofNullable(bufferedReader).ifPresent(this::closeReader);
               connectedClients.remove(this);})
             .onFailure(throwable -> log.info("Couldnt close connection: " + throwable));
     }

     private void closeReader(BufferedReader reader) {
          Try.run(reader::close)
             .onFailure(throwable -> log.error("Could not close reader: " + throwable));
     }

     private MessageDTO decryptMessage(MessageDTO msg) {
          String decryptedPayload = EncryptionWorker.ofPayload(msg.getMsg())
                                           .base64Decode()
                                           .decrypt(encryption, protocolParameters.getCommonEncKey())
                                           .getPayload();
          return MessageDTO.builder()
                           .from(msg.getFrom())
                           .msg(decryptedPayload)
                           .build();
     }

     private void encryptAndSendMessage(MessageDTO msg) throws JsonProcessingException {
          String encryptedMsg =
                  EncryptionWorker.ofPayload(msg.getMsg())
                                  .encrypt(encryption, protocolParameters.getCommonEncKey())
                                  .base64Encode()
                                  .getPayload();
          MessageDTO payload = MessageDTO.builder()
                                         .from(msg.getFrom())
                                         .msg(encryptedMsg)
                                         .build();

          send(payload);
     }

     private void setUpEncryption(String encryptionRequest) throws IOException {
          EncryptionDTO encryptionDTO = objectMapper.readValue(encryptionRequest, EncryptionDTO.class);
          log.info("Changing encryption to: "+ encryptionDTO.getEncryption());
          this.encryption = Encryption.findByName(encryptionDTO.getEncryption());
     }

     private void send(Object payload) throws JsonProcessingException {
          String payloadJson = objectMapper.writeValueAsString(payload);
          send(payloadJson);
     }

     private void send(String line) {
          this.printStream.println(line);
     }
}
