package encryption;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OtpCipherTest {

     @Nested
     @DisplayName("Given string to encrypt")
     class encrypt {

          @Test
          @DisplayName("encrypted should differ from original")
          void shouldDiffer() {
               String toEnc = "asdfa";
               long key = 123;
               String encrypted = OtpCipher.encrypt(toEnc, key);
               assertNotEquals(toEnc, encrypted);
          }
     }

     @Nested
     @DisplayName("Given string to decrypt")
     class decrypt {

          @Test
          @DisplayName("decrypted should equals original")
          void shouldDiffer() {
               String toEnc = "abcdefghijklmno";
               long key = 12;
               String encrypted = OtpCipher.encrypt(toEnc, key);
               String decrypted = OtpCipher.decrypt(encrypted, key);
               assertEquals(toEnc, decrypted);
          }
     }
}