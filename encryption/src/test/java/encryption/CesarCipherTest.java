package encryption;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CesarCipherTest {

     @Nested
     @DisplayName("Given string to encrypt")
     class Encrypt {

          @Test
          @DisplayName("Encrypted should differ from original")
          void shouldDiffer() throws Exception {
               String toEncrypt = "abc d";
               long key = 1;
               String encrypted = CesarCipher.encrypt(toEncrypt, key);
               assertNotEquals(toEncrypt, encrypted);
          }

          @Test
          @DisplayName("Encrypted should have same length")
          void shouldHaveSameLength() throws Exception {
               String toEncrypt = "abc d";
               long key = 1;
               String encrypted = CesarCipher.encrypt(toEncrypt, key);
               assertEquals(toEncrypt.length(), encrypted.length());
          }

          @Test
          @DisplayName("None of characters should remain the same")
          void changeEveryChar() {
               String toEncrypt = "1234567890qwertyuiopasdfghjkl;zxcvbnm,./  !@#$%^&*()_";
               long key = 1;
               String encrypted = CesarCipher.encrypt(toEncrypt, key);
               int length = toEncrypt.length();
               for(int i = 0; i < length; i++) {
                    assertNotEquals(toEncrypt.codePointAt(i), encrypted.codePointAt(i));
               }
          }
     }

     @Nested
     @DisplayName("Given string to decrypt")
     class Decrypt {

          @Test
          @DisplayName("Decrypted should be equal to original")
          public void encrypt() throws Exception {
               String toEncrypt = "abc d";
               long key = 1;
               String encrypted = CesarCipher.encrypt(toEncrypt, key);
               String decrypted = CesarCipher.decrypt(encrypted, key);
               assertEquals(toEncrypt, decrypted);
          }
     }
}