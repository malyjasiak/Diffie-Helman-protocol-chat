package encryption.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RandomGeneratorTest {

     @Test
     @DisplayName("Should find a root for every prime")
     void generatePrimitiveRootModulo() {
          for (long prime : RandomGenerator.getPRIMES()) {
               long l = RandomGenerator.generatePrimitiveRootModulo(prime);
               assertTrue(l > 1);
               assertTrue(l < prime);
          }
     }

}