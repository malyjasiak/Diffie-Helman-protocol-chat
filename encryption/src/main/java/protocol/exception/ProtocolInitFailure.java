package protocol.exception;

public class ProtocolInitFailure extends RuntimeException {
     public static final String MESSAGE = "Protocol initialization failure";

     public ProtocolInitFailure() {
          super(MESSAGE);
     }

     public ProtocolInitFailure(String message) {
          super(message);
     }
}
