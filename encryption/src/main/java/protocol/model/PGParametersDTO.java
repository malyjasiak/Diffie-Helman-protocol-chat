package protocol.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class PGParametersDTO {

     private Long p;
     private Long g;
}
