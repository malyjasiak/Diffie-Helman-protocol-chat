package protocol.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import protocol.exception.ProtocolInitFailure;

@Getter
@Setter
@NoArgsConstructor
@Slf4j
public class ProtocolParams {

     private Long p;
     private Long g;
     private Long privateKey;
     private Long publicKey;
     private Long receivedKey;
     private Long commonEncKey;

     public void setPG(PGParametersDTO pgParametersDTO) {
          Long g = pgParametersDTO.getG();
          Long p = pgParametersDTO.getP();
          if (g == null || p == null) {
               throw new ProtocolInitFailure("Missing PG params");
          }

          this.setP(p);
          this.setG(g);
          log.info("p g params set to [" + p + "; " + g + "]");
     }

     public void setReceivedKey(Long key) {
          if (key == null) {
               throw new ProtocolInitFailure("B cannot be null");
          }
          this.receivedKey = key;
          log.info("ReceivedKey param set to " + key);
     }

     public boolean isReadyToForSending() {
          return this.commonEncKey != null;
     }
}
