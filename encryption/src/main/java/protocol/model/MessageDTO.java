package protocol.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MessageDTO {

     private String msg;
     private String from;

     public String toPrettyString() {
          return from + ": " + msg;
     }
}
