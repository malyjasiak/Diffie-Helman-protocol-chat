package encryption.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class EncryptionDTO {

     private String encryption;
}
