package encryption.common;

public class EncryptionNames {
     public static final String ENCRYPTION_NONE_VALUE = "none";
     public static final String ENCRYPTION_CESAR_VALUE = "cezar";
     public static final String ENCRYPTION_XOR_VALUE = "xor";
}
