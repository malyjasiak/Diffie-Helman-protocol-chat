package encryption.common;

import lombok.Getter;

import java.math.BigInteger;
import java.util.Random;

import static java.lang.Math.*;

public class RandomGenerator {

     @Getter
     private static final long[] PRIMES =
             {
                       3,   5,   7,  11,  13,  17,  19,  23,  29,
                      31,  37,  41,  43,  47,  53,  59,  61,  67,  71,
                      73,  79,  83,  89,  97, 101, 103, 107, 109, 113
             };
     private static final int PRIMES_COUNT = 19;
     private static final long MAX_RAND = 100L;
     private static final Random RAND = new Random();

     public static long generatePrimeLong() {
          return PRIMES[abs(RAND.nextInt() % PRIMES_COUNT)];
     }

     public static long generatePositiveLong() {
          return abs(RAND.nextLong() % MAX_RAND) + 1;
     }

     public static long generatePrimitiveRootModulo(long m) {
          BigInteger mBig = BigInteger.valueOf(m);
          long gcd = -1L;
          long root = 0L;
          while(gcd != 1) {
               root = Math.abs(RAND.nextLong()) % (m - 2) + 2;
               gcd = BigInteger.valueOf(root)
                               .gcd(mBig)
                               .longValue();
          }
          return root;
     }
}
