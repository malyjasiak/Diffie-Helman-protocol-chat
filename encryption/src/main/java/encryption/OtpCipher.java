package encryption;

import encryption.common.StreamUtils;

public class OtpCipher {

     public static String encrypt(String payload, long key) {
          byte secretByte = (byte) key;
          return payload.chars()
                        .mapToObj(i -> (byte) i)
                        .map(b -> b ^ secretByte)
                        .collect(StreamUtils.intsToString());
     }

     public static String decrypt(String payload, long key) {
          return encrypt(payload, key);
     }
}
