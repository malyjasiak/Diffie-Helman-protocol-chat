package encryption;

import encryption.model.Encryption;
import lombok.Getter;

import java.util.Base64;

public class EncryptionWorker {

     @Getter
     private String payload;

     private EncryptionWorker(String payload) {
          this.payload = payload;
     }

     public static EncryptionWorker ofPayload(String payload) {
          return new EncryptionWorker(payload);
     }

     public EncryptionWorker encrypt(Encryption encryption, long key) {
          this.payload = encryptPayload(encryption, key);
          return this;
     }

     private String encryptPayload(Encryption encryption, long key) {
          switch(encryption) {
               case XOR: return OtpCipher.encrypt(payload, key);
               case Cesar: return CesarCipher.encrypt(payload, key);
               default: return payload;
          }
     }

     public EncryptionWorker decrypt(Encryption encryption, long key) {
          this.payload = decryptPayload(encryption, key);
          return this;
     }

     private String decryptPayload(Encryption encryption, long key) {
          switch(encryption) {
               case XOR: return OtpCipher.decrypt(payload, key);
               case Cesar: return CesarCipher.decrypt(payload, key);
               default: return payload;
          }
     }

     public EncryptionWorker base64Encode() {
          this.payload = new String(Base64.getEncoder().encode(payload.getBytes()));
          return this;
     }

     public EncryptionWorker base64Decode() {
          this.payload = new String(Base64.getDecoder().decode(payload.getBytes()));
          return this;
     }
}
