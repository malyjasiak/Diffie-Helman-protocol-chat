package encryption;


import encryption.common.StreamUtils;

public class CesarCipher {

     public static String encrypt(String payload, long key) {
          return payload.codePoints()
                        .boxed()
                        .map(c -> shift(c, key))
                        .collect(StreamUtils.intsToString());
     }

     public static String decrypt(String payload, long key) {
          return encrypt(payload, -key);
     }

     private static int shift(int character, long key) {
          long shiftAmount = key % Integer.MAX_VALUE;
          long shiftedChar = (long) character + shiftAmount;
          return (int)((shiftedChar + Integer.MAX_VALUE) % Integer.MAX_VALUE);
     }
}
