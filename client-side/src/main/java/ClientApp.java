import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ClientApp extends Application {

     private static final String HOME_VIEW_PATH = "ClientView.fxml";
     private static final int INITIAL_WIDTH = 600;
     private static final int INITIAL_HEIGHT = 400;

     public static void main(String[] args) {
          launch(args);
     }

     public void start(Stage primaryStage) throws Exception {
          Parent root = FXMLLoader.load(getClass().getResource(HOME_VIEW_PATH));
          primaryStage.setTitle("Client");
          primaryStage.setScene(new Scene(root, INITIAL_WIDTH, INITIAL_HEIGHT));
          primaryStage.show();
     }
}
