package chat;

public class Common {

     public static final String REQUEST_TYPE_KEY = "request";
     public static final String KEYS_REQUEST_VALUE = "keys";
     public static final String P_KEY = "p";
     public static final String G_KEY = "g";
     public static final String A_KEY = "a";
     public static final String B_KEY = "b";
     public static final String ENCRYPTION_KEY = "encryption";
     public static final String ENCRYPTION_NONE_VALUE = "none";
     public static final String ENCRYPTION_CESAR_VALUE = "cezar";
     public static final String ENCRYPTION_XOR_VALUE = "xor";
     public static final String MESSAGE_KEY = "msg";
     public static final String FROM_KEY = "from";
     public static final long MAX_RAND = 15;

     private Common() {
     }
}
