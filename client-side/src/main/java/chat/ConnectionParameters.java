package chat;

import encryption.model.Encryption;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class ConnectionParameters {

     private String host;
     private Integer port;
     private String username;
     private Encryption encryption;
}
