# Multi-client chat over Sockets with Diffie Hellman protocol support

## Functionalities
Application, as separate submodules, is capable of handling simple communitcator features with encryption support. 

## Project requirements:
1. JDK 1.8 (developed under 151 patch) with bundled javafx libraries (i.e. Oracle, OpenJDK).
2. Maven 3.5.0

## Project structure:
Project consists of three modules: client-side, server-side and encryption. While meaning of the first two is obvious, the last one has to be further described. Module _encryption_ contains common utils for protocol handling and both of remaining modules depend on it. This implies necessity for compiling it before them. Luckily, Maven handles it. Client-side is a client chat app with GUI that allows for setting up a connection to chat server. __All params are to be set in runtime__. Encryption can be changed on-the-fly. Server-side is a chat server. User has to provide only a port number to listen at (in runtime).

## Build and run
Assuming all the requirements are met, procedure is as follows:
### Build:
1. cd to project root directory
2. `mvn clean install`
3. Sources compiled to jar are located in `./<module-directory>/target/<module-name>-<version>-jar-with-dependencies.jar`

### Run:

There are two runnable jars. One for client application and another for server application.

1. `java -jar ./<module-directory>/target/<module-name>-<version>-jar-with-dependencies.jar`

	i.e. `java -jar server-side/target/server-1.0-SNAPSHOT-jar-with-dependencies.jar`
